import { Headers } from '@angular/http';

const server_name = 'http://localhost:8000/';

let headers = new Headers();


export const global = {

    HEADERS: headers,
    SERVER_NAME: server_name,

    // Auth
    API_LOGIN: server_name + 'auth/api/login/',
    API_LOGOUT: server_name + 'auth/api/logout/',
    API_REGISTER: server_name + 'users/api/create-client/',

    // GET DATA
    API_CITIES: server_name + 'cities/api/list/ ',
};