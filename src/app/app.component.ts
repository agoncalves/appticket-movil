import { Component, ViewChild } from '@angular/core';
import { Platform, Nav, App } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

// Pages
import { TabsPage } from '../pages/tabs/tabs';
import { LoginPage } from '../pages/login/login';

// Services
import { AuthService } from '../providers/authentication.service';


@Component({
  templateUrl: 'app.html'
})
export class MyApp {

  @ViewChild(Nav) nav: Nav; 

  rootPage:any;
  root: any
  tabsPage:any = TabsPage;
  loginPage:any = LoginPage;

  constructor(
    platform: Platform, 
    statusBar: StatusBar, 
    splashScreen: SplashScreen,
    public app: App,
    private authService: AuthService
  ) {
    platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.

      // if user is logged in, go to the tabs page
      if ( window.localStorage.getItem('token') != null &&
        window.localStorage.getItem('token') != ''
      ) {
        this.rootPage = this.tabsPage;
      }
      else {
        this.rootPage = this.loginPage;
      }

      statusBar.styleDefault();
      splashScreen.hide();
    });
  }

  onLogout() {

    this.authService.logout()
    .then(data => {
      this.root = this.app.getRootNavById("n4"); 
      this.root.setRoot(LoginPage);

      window.localStorage.removeItem('token');
      window.localStorage.clear();
    },
    error => {
      console.log(error);
    });
    
  }

}
