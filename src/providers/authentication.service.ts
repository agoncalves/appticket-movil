import { Injectable } from '@angular/core';
import { Http, Headers, RequestOptions } from '@angular/http';
import 'rxjs/add/operator/map';

import { global } from "../app/global";


@Injectable()
export class AuthService {

    /**
     * Services that holds the login and logout logic.
     *
     * @access Public
     */
    constructor (
        public http: Http,
    ) {
    }

    /**
     * Sends a POST request to login a given user.
     *
     * @access Public
     * @param user: an object that holds the email and password.
     */
    login(user: any) {
        let headers = new Headers({
            "Accept": 'application/json',
            "Content-Type": 'application/json'
        });

        let options = new RequestOptions({ headers: headers });
        let postParams = user;

        return this.http.post(global.API_LOGIN, postParams, options)
            .map(data => data.json())
            .toPromise();
    }

    /**
     * Sends a POST request to login a given user.
     *
     * @access Public
     */
    logout() {
        let headers = new Headers({
            "Accept": 'application/json',
            "Content-Type": 'application/json',
            "Authorization": 'Token ' + window.localStorage.getItem('token'),
        });

        let options = new RequestOptions({ headers: headers });

        return this.http.get(global.API_LOGOUT, options)
            .toPromise();
    }

    /**
     * Sends a POST request to register a user.
     *
     * @access Public
     * @param user: an object that holds the email and password.
     */
    register(user: any) {
        let headers = new Headers({
            "Accept": 'application/json',
            "Content-Type": 'application/json'
        });

        let options = new RequestOptions({ headers: headers });
        let postParams = { user };

        return this.http.post(global.API_REGISTER, postParams, options)
            .map(data => data.json())
            .toPromise();
    }

}