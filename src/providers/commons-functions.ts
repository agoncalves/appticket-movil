import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';

import { global } from "../app/global";


@Injectable()
export class GetService {

    /**
     * Services that holds the login and logout logic.
     *
     * @access Public
     */
    constructor (
        public http: Http,
    ) {
    }

    /**
     * Sends a GET request to cities.
     *
     * @access Public
     */
    Cities() {
		return this.http.get(global.API_CITIES)
	      .map(res => res.json())
	      .toPromise()
    }

}