import { Injectable } from '@angular/core';
import { ToastController } from "ionic-angular";
import 'rxjs/add/operator/map';


@Injectable()
export class ErrorFunctions {

    /**
     * Services that holds the login and logout logic.
     *
     * @access Public
     */
    constructor (
        private toastCtrl: ToastController,
    ) {

    }

    error404() {
        let toast = this.toastCtrl.create({
            message: "Page is unavailable (404)",
            duration: 3000,
            position: 'top'
        });
        toast.present();
    }

    error400() {
        let toast = this.toastCtrl.create({
            message: "Something unexpected happened (400)",
            duration: 3000,
            position: 'top',
        });
        toast.present();
    }

    error0() {
        let toast = this.toastCtrl.create({
            message: "Make sure your phone has an Internet connection",
            duration: 3000,
            position: 'top'
        });
        toast.present();
    }

    error500() {
        let toast = this.toastCtrl.create({
            message: "Server is unavailable (500)",
            duration: 3000,
            position: 'top'
        });
        toast.present();
    }

    generalError(message: string) {
        let toast = this.toastCtrl.create({
            message: message,
            duration: 3000,
            position: 'top',
            cssClass: 'toast-message',
        });
        toast.present();
    }


    errors(error: any){
        if (error.status == 404) {
            this.error404();
        }
        else if (error.status == 400) {
            let errorDetail = JSON.parse(error._body);
            if (errorDetail.non_field_errors) {
                this.generalError(errorDetail.non_field_errors[0]);
            } 
            else if (errorDetail.username) {
                this.generalError(errorDetail.username[0]);
            } 
            else if (errorDetail.password) {
                this.generalError(errorDetail.password[0]);
            } 
            else if (errorDetail.email) {
                this.generalError(errorDetail.email[0]);
            } 
            else {
                this.error400();
            }
        }
        else if (error.status == 500) {
            this.error500();
        }
        else if (error.status == 0) {
            this.error0();
        }
        else {
            let errorDetail = JSON.parse(error._body);
            this.generalError(errorDetail.detail);
        }
    }
}
