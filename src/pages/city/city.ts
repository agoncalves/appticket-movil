import { Component, OnInit } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';

// Service
import { GetService } from '../../providers/commons-functions';



@Component({
  selector: 'page-city',
  templateUrl: 'city.html',
})
export class CityPage implements OnInit {

  constructor(
  	public navCtrl: NavController, 
  	public navParams: NavParams,
  	public getService: GetService ) {
  }

  ngOnInit(){
  	this.loadCities()
  }

  loadCities() {
  	this.getService.Cities()
  	.then( data => console.log(data) )
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CityPage');
  }

}
