import { Component } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

// Service
import { AuthService } from '../../providers/authentication.service';
import { ErrorFunctions } from '../../providers/errors';

// Page
import { TabsPage } from '../tabs/tabs';
import { RegisterPage } from '../register/register';


/**
 * Generated class for the LoginPage page.
 *
 */

@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {

	loginForm: FormGroup;

	constructor(
		public navCtrl: NavController, 
		public navParams: NavParams,
        private errorsFunction: ErrorFunctions,
		private formBuilder: FormBuilder,
		private authService: AuthService) {

		this.loginForm = this.formBuilder.group({
            username: ['',
                Validators.compose([
                    Validators.maxLength(70),
                    Validators.pattern(
                        '^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$'
                    ),
                    Validators.required
                ])
            ],
            password: ['', [ Validators.required ] ],
        });
	}

    /** Function that sends login form
     */
    onSubmit(){

/*      console.log(this.loginForm.value)
        console.log(this.loginForm.value.username)
        console.log(this.loginForm)
        console.log(this.loginForm.controls['username'].errors)   */ 
    	this.authService.login(this.loginForm.value)
        .then(data => {

            // Set logged user
            window.localStorage.removeItem("token");
            window.localStorage.setItem('token', data.token);
            
        	// Redirects to tabs page
            this.navCtrl.push(TabsPage);
        },
        error => {
            // TODO: Handle error.
            console.log(error);
            this.errorsFunction.errors(error);
        });
    }

    onRegister() {
        this.navCtrl.push(RegisterPage)
    }
}
