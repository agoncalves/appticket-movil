import { Component, /*OnInit*/ } from '@angular/core';
import { NavController, NavParams } from 'ionic-angular';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

// Service
import { AuthService } from '../../providers/authentication.service';
import { ErrorFunctions } from '../../providers/errors';


@Component({
    selector: 'page-register',
    templateUrl: 'register.html',
})

export class RegisterPage {

    registerForm: FormGroup;

    // Load
    public cities: any;

    constructor(
        public navCtrl: NavController, 
        public navParams: NavParams,
        public formBuilder: FormBuilder,
        public errorsFunction: ErrorFunctions,
        private authService: AuthService) {

        this.registerForm = this.formBuilder.group({
            first_name: ['', [ Validators.required ] ],
            last_name: ['', [ Validators.required ] ],
            email: ['',
                Validators.compose([
                    Validators.maxLength(70),
                    Validators.pattern(
                        '^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$'
                    ),
                    Validators.required
                ])
            ],
            password: ['', [ Validators.required ] ],
            confirm_password: ['', [ Validators.required ] ],
        });
    }

    onLogin() {
        this.navCtrl.pop()
    }

    onSubmit() {
        this.authService.register(this.registerForm.value)
        .then(data => {
            console.log(data)
        },
        error => {
            // TODO: Handle error.
            console.log(error);
            this.errorsFunction.errors(error);
        });
    }

}

